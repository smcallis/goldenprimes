#include <time.h>
#include <primes.h>

#include <stdio.h>
#include <stdint.h>

static inline uint64_t rdtscp() {
    uint64_t rax,rdx;
    asm volatile ( "rdtscp\n" : "=a" (rax), "=d" (rdx) : : );
    return (rdx << 32) + rax;
}

double stopwatch() {
    static timespec last;
    timespec curr;
    clock_gettime(CLOCK_MONOTONIC, &curr);
    double elapsed = (double)(curr.tv_sec - last.tv_sec) + (double)(curr.tv_nsec - last.tv_nsec)*1e-9;
    last = curr;
    return elapsed;
}


__attribute__((noinline))
double bench_masking(volatile uint64_t *values, size_t nvalue, uint64_t mask) {
    stopwatch();
    uint64_t tsc = rdtscp();
    for (size_t ii=0; ii < nvalue; ii++) {
        values[0] = values[0] & mask;
    }
    tsc = rdtscp() - tsc;
    double end=stopwatch();
    printf("time for mask - %.6f (%e samp/sec  %.6e/iter  %.3f cyc/iter)\n", end, nvalue/end, end/nvalue, (double)tsc/nvalue);
    return end;
}


__attribute__((noinline))
double bench_modulus(volatile uint64_t *values, size_t nvalue, uint64_t mod, double baseline, bool quiet) {
    stopwatch();    
    uint64_t tsc = rdtscp();
    for (size_t ii=0; ii < nvalue; ii++) {
        values[0] = values[0] % mod;
    }
    tsc = rdtscp() - tsc;
    double end=stopwatch();

    if (!quiet) { 
        printf("time for mod  - %.6f (%e samp/sec  %.6e/iter  %.3f cyc/iter) %.2f\n", end, nvalue/end, end/nvalue, (double)tsc/nvalue, end/baseline);
    }
    return end;
}


__attribute__((noinline))
double bench_prime(const size_t pp, volatile uint64_t *values, size_t nvalue, double baseline) {
    stopwatch();
    uint64_t tsc  = rdtscp();
    for (size_t ii=0; ii < nvalue; ii++) {
        values[0] = prime_modulus(pp, values[0]);
    }
    tsc = rdtscp() - tsc;
    double end=stopwatch();
    printf("time for %3zd  - %.6f (%e samp/sec  %.6e/iter  %.3f cyc/iter) %.2f %zu\n", pp, end, nvalue/end, end/nvalue, (double)tsc/nvalue, end/baseline, prime_sizes[pp]);
    return end;
}

static inline uint64_t knuthmod(uint64_t nn, uint64_t mm) {
    const uint64_t alpha = 11400712942368927719ull; // largest prime < 2**64/phi
    return (((__uint128_t)mm*(alpha*nn)) >> 64);
}

__attribute__((noinline))
double bench_knuth(const size_t pp, volatile uint64_t *values, size_t nvalue, double baseline) {
    stopwatch();
    uint64_t size = prime_sizes[pp];
    uint64_t tsc  = rdtscp();
    for (size_t ii=0; ii < nvalue; ii++) {
        values[0] = knuthmod(values[0], size);
    }
    tsc = rdtscp() - tsc;
    double end=stopwatch();
    printf("time for %3zd  - %.6f (%e samp/sec  %.6e/iter  %.3f cyc/iter) %.2f %zu\n", pp, end, nvalue/end, end/nvalue, (double)tsc/nvalue, end/baseline, prime_sizes[pp]);
    return end;
}
