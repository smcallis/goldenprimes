//-*-c++-*-
#pragma once

#include <stdint.h>
#include <unistd.h>
#include <cassert>

static inline uint64_t prime_modulus(size_t pp, uint64_t nn) {
    switch(pp) {
        case   0: return nn %                   13ull; break; // 1.000000000000 ratio
        case   1: return nn %                   19ull; break; // 1.461538461538 ratio
        case   2: return nn %                   37ull; break; // 1.947368421053 ratio
        case   3: return nn %                   73ull; break; // 1.972972972973 ratio
        case   4: return nn %                  113ull; break; // 1.547945205479 ratio
        case   5: return nn %                  179ull; break; // 1.584070796460 ratio
        case   6: return nn %                  283ull; break; // 1.581005586592 ratio
        case   7: return nn %                  421ull; break; // 1.487632508834 ratio
        case   8: return nn %                  631ull; break; // 1.498812351544 ratio
        case   9: return nn %                  947ull; break; // 1.500792393027 ratio
        case  10: return nn %                 1423ull; break; // 1.502639915523 ratio
        case  11: return nn %                 2131ull; break; // 1.497540407590 ratio
        case  12: return nn %                 3191ull; break; // 1.497419052088 ratio
        case  13: return nn %                 4793ull; break; // 1.502036979003 ratio
        case  14: return nn %                 7193ull; break; // 1.500730231588 ratio
        case  15: return nn %                10789ull; break; // 1.499930487974 ratio
        case  16: return nn %                16141ull; break; // 1.496060802669 ratio
        case  17: return nn %                24251ull; break; // 1.502447184189 ratio
        case  18: return nn %                36389ull; break; // 1.500515442662 ratio
        case  19: return nn %                54617ull; break; // 1.500920607876 ratio
        case  20: return nn %                81929ull; break; // 1.500064082612 ratio
        case  21: return nn %               122867ull; break; // 1.499676549207 ratio
        case  22: return nn %               184333ull; break; // 1.500264513661 ratio
        case  23: return nn %               276503ull; break; // 1.500018987376 ratio
        case  24: return nn %               414763ull; break; // 1.500030741077 ratio
        case  25: return nn %               622133ull; break; // 1.499972273322 ratio
        case  26: return nn %               933209ull; break; // 1.500015270047 ratio
        case  27: return nn %              1399813ull; break; // 1.499999464214 ratio
        case  28: return nn %              2099731ull; break; // 1.500008215383 ratio
        case  29: return nn %              3149599ull; break; // 1.500001190629 ratio
        case  30: return nn %              4724381ull; break; // 1.499994443737 ratio
        case  31: return nn %              7086593ull; break; // 1.500004550861 ratio
        case  32: return nn %             10629917ull; break; // 1.500003880567 ratio
        case  33: return nn %             15944839ull; break; // 1.499996566295 ratio
        case  34: return nn %             23917301ull; break; // 1.500002665439 ratio
        case  35: return nn %             35875969ull; break; // 1.500000731688 ratio
        case  36: return nn %             53813933ull; break; // 1.499999428587 ratio
        case  37: return nn %             80720929ull; break; // 1.500000548185 ratio
        case  38: return nn %            121081339ull; break; // 1.499999324834 ratio
        case  39: return nn %            181622069ull; break; // 1.500000499664 ratio
        case  40: return nn %            272433127ull; break; // 1.500000129390 ratio
        case  41: return nn %            408649601ull; break; // 1.499999671479 ratio
        case  42: return nn %            612974539ull; break; // 1.500000336474 ratio
        case  43: return nn %            919461721ull; break; // 1.499999857253 ratio
        case  44: return nn %           1379192663ull; break; // 1.500000088639 ratio
        case  45: return nn %           2068789081ull; break; // 1.500000062718 ratio
        case  46: return nn %           3103183649ull; break; // 1.500000013293 ratio
        case  47: return nn %           4654775483ull; break; // 1.500000003061 ratio
        case  48: return nn %           6982163159ull; break; // 1.499999985928 ratio
        case  49: return nn %          10473244783ull; break; // 1.500000006373 ratio
        case  50: return nn %          15709867177ull; break; // 1.500000000239 ratio
        case  51: return nn %          23564800859ull; break; // 1.500000005952 ratio
        case  52: return nn %          35347201321ull; break; // 1.500000001379 ratio
        case  53: return nn %          53020801903ull; break; // 1.499999997779 ratio
        case  54: return nn %          79531202921ull; break; // 1.500000001254 ratio
        case  55: return nn %         119296804457ull; break; // 1.500000000949 ratio
        case  56: return nn %         178945206697ull; break; // 1.500000000096 ratio
        case  57: return nn %         268417810043ull; break; // 1.499999999986 ratio
        case  58: return nn %         402626715083ull; break; // 1.500000000069 ratio
        case  59: return nn %         603940072639ull; break; // 1.500000000036 ratio
        case  60: return nn %         905910108931ull; break; // 1.499999999954 ratio
        case  61: return nn %        1358865163433ull; break; // 1.500000000040 ratio
        case  62: return nn %        2038297745051ull; break; // 1.499999999928 ratio
        case  63: return nn %        3057446617633ull; break; // 1.500000000028 ratio
        case  64: return nn %        4586169926587ull; break; // 1.500000000045 ratio
        case  65: return nn %        6879254889733ull; break; // 1.499999999968 ratio
        case  66: return nn %       10318882334809ull; break; // 1.500000000030 ratio
        case  67: return nn %       15478323501929ull; break; // 1.499999999972 ratio
        case  68: return nn %       23217485253329ull; break; // 1.500000000028 ratio
        case  69: return nn %       34826227879991ull; break; // 1.500000000000 ratio
        case  70: return nn %       52239341819971ull; break; // 1.500000000000 ratio
        case  71: return nn %       78359012729993ull; break; // 1.500000000001 ratio
        case  72: return nn %      117538519095001ull; break; // 1.500000000000 ratio
        case  73: return nn %      176307778642507ull; break; // 1.500000000000 ratio
        case  74: return nn %      264461667963589ull; break; // 1.499999999999 ratio
        case  75: return nn %      396692501945657ull; break; // 1.500000000001 ratio
        case  76: return nn %      595038752918443ull; break; // 1.500000000000 ratio
        case  77: return nn %      892558129377727ull; break; // 1.500000000000 ratio
        case  78: return nn %     1338837194066329ull; break; // 1.500000000000 ratio
        case  79: return nn %     2008255791099659ull; break; // 1.500000000000 ratio
        case  80: return nn %     3012383686649803ull; break; // 1.500000000000 ratio
        case  81: return nn %     4518575529974813ull; break; // 1.500000000000 ratio
        case  82: return nn %     6777863294962153ull; break; // 1.500000000000 ratio
        case  83: return nn %    10166794942443353ull; break; // 1.500000000000 ratio
        case  84: return nn %    15250192413664969ull; break; // 1.500000000000 ratio
        case  85: return nn %    22875288620497549ull; break; // 1.500000000000 ratio
        case  86: return nn %    34312932930746291ull; break; // 1.500000000000 ratio
        case  87: return nn %    51469399396119389ull; break; // 1.500000000000 ratio
        case  88: return nn %    77204099094179219ull; break; // 1.500000000000 ratio
        case  89: return nn %   115806148641268823ull; break; // 1.500000000000 ratio
        case  90: return nn %   173709222961903373ull; break; // 1.500000000000 ratio
        case  91: return nn %   260563834442855131ull; break; // 1.500000000000 ratio
        case  92: return nn %   390845751664282661ull; break; // 1.500000000000 ratio
        case  93: return nn %   586268627496424027ull; break; // 1.500000000000 ratio
        case  94: return nn %   879402941244636143ull; break; // 1.500000000000 ratio
        case  95: return nn %  1319104411866954161ull; break; // 1.500000000000 ratio
        case  96: return nn %  1978656617800431347ull; break; // 1.500000000000 ratio
        case  97: return nn %  2967984926700646891ull; break; // 1.500000000000 ratio
        case  98: return nn %  4451977390050970579ull; break; // 1.500000000000 ratio
        case  99: return nn %  6677966085076455397ull; break; // 1.500000000000 ratio
        case 100: return nn % 10016949127614683131ull; break; // 1.500000000000 ratio
        case 101: return nn % 15025423691422023641ull; break; // 1.500000000000 ratio
        case 102: return nn % 18446744073709551557ull; break; // 1.227702090307 ratio
        default: assert(false && "unknown prime factor");
    }
}


static const size_t   nprimes = 103;
static const uint64_t prime_sizes[nprimes] = {
    13ull,                    19ull,                    37ull,                    73ull,                    
    113ull,                   179ull,                   283ull,                   421ull,                   
    631ull,                   947ull,                   1423ull,                  2131ull,                  
    3191ull,                  4793ull,                  7193ull,                  10789ull,                 
    16141ull,                 24251ull,                 36389ull,                 54617ull,                 
    81929ull,                 122867ull,                184333ull,                276503ull,                
    414763ull,                622133ull,                933209ull,                1399813ull,               
    2099731ull,               3149599ull,               4724381ull,               7086593ull,               
    10629917ull,              15944839ull,              23917301ull,              35875969ull,              
    53813933ull,              80720929ull,              121081339ull,             181622069ull,             
    272433127ull,             408649601ull,             612974539ull,             919461721ull,             
    1379192663ull,            2068789081ull,            3103183649ull,            4654775483ull,            
    6982163159ull,            10473244783ull,           15709867177ull,           23564800859ull,           
    35347201321ull,           53020801903ull,           79531202921ull,           119296804457ull,          
    178945206697ull,          268417810043ull,          402626715083ull,          603940072639ull,          
    905910108931ull,          1358865163433ull,         2038297745051ull,         3057446617633ull,         
    4586169926587ull,         6879254889733ull,         10318882334809ull,        15478323501929ull,        
    23217485253329ull,        34826227879991ull,        52239341819971ull,        78359012729993ull,        
    117538519095001ull,       176307778642507ull,       264461667963589ull,       396692501945657ull,       
    595038752918443ull,       892558129377727ull,       1338837194066329ull,      2008255791099659ull,      
    3012383686649803ull,      4518575529974813ull,      6777863294962153ull,      10166794942443353ull,     
    15250192413664969ull,     22875288620497549ull,     34312932930746291ull,     51469399396119389ull,     
    77204099094179219ull,     115806148641268823ull,    173709222961903373ull,    260563834442855131ull,    
    390845751664282661ull,    586268627496424027ull,    879402941244636143ull,    1319104411866954161ull,   
    1978656617800431347ull,   2967984926700646891ull,   4451977390050970579ull,   6677966085076455397ull,   
    10016949127614683131ull,  15025423691422023641ull,  18446744073709551557ull,  
};
