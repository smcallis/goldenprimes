#!/usr/bin/env python

import os
import sys
import time
import math

while (True):
    print "generate primes"
    os.system("./goldenprimes disallowed > primes.h 2> /dev/null")
    print "benchmark"
    os.system("g++ -I. primetest.cc -O3 -o primetest")
    os.system("taskset -c 2 ./primetest > times");

    # open times file and parse out any that weren't great
    print "find any failures"
    dset = set()
    for line in open("disallowed", "r"):
        dset.add(int(line.strip()))
        
    # read timings and compute mean/variance
    entries = []
    n    = 0
    mean = 0.0
    m2   = 0.0
    for line in open("times", "r"):
        tokens = line.split()
        if (len(tokens) != 10):
            continue
        ratio = float(tokens[-2])
        num   = int(tokens[-1])
        
        entries.append((ratio, num))

        # update variance/mean
        n += 1
        delta = ratio - mean;
        mean += delta/n
        m2   += delta*(ratio - mean)

    sigma = math.sqrt(m2/(n-1))

    # sometimes we get runs that have odd timings...
    print "mean: %.2f  sigma: %.6f" % (mean, sigma)

    if (mean > 2.5):
        time.sleep(1)
        continue
    

    ndisallow = 0
    for ratio,num in entries:
        if ((ratio-mean)/sigma >= 1):
            if (not num in dset):
                dset.add(num)
                ndisallow += 1
                
    if (ndisallow == 0):
        break

    print "%i disallowed, %i new" % (len(dset), ndisallow)
    print "\n"

    out = open("disallowed", "w")
    for num in sorted(list(dset)):
        out.write("%i\n" % num)
    out.close()
    
print "done"
