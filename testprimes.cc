#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#include <primes.h>

int main() {
    srand(time(NULL));
    
    for (size_t ii=0; ii < nprimes; ii++) {
        for (size_t nn=0; nn < 8*1024*1024; nn++) {
            uint64_t num = (uint64_t)rand() << 32 | rand();
            
            if (prime_modulus(ii, num) >= prime_sizes[ii]) {//!= num % prime_sizes[ii]) {
                fprintf(stderr, "failured for %llu %% %llu   %zu != %zu\n",
                    (long long unsigned)num, (long long unsigned)prime_sizes[ii],
                    prime_modulus(ii, num), (num % prime_sizes[ii])
                );
                exit(EXIT_FAILURE);
            }
        }
        printf("%zu ok\n", prime_sizes[ii]);
    }
    printf("all good!\n");
}
