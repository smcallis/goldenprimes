#include <primes.h>

#include <cmath>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

bool prime(uint64_t nn) {
    if (nn % 2 == 0) return false;
    uint64_t limit = (uint64_t)ceil(sqrt((long double)nn));
    for (uint64_t ii=3; ii < limit; ii += 2) {
        if (nn % ii == 0) return false;
    }
    return true;
}


int main() {
    for (size_t ii=0; ii < nprimes; ii++) {
        printf("%3zd testing %llu - ", ii, (unsigned long long)prime_sizes[ii]);

        if (prime(prime_sizes[ii])) {
            printf("prime\n");
        } else {
            printf("composite\n");
        }
    }
}
