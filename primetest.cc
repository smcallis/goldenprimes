#include <cassert>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <time.h>

#include <bench.h>

int main() {
    srand(time(NULL));

    size_t nvalue = 16*1024*1024;
    volatile uint64_t value = (uint64_t)rand() << 32 | rand();

    // do something up front to run up the CPU freq
    bench_modulus(&value, 32*1024*1024, (uint64_t)rand() << 32 | rand(), 1.0, true);

    // now benchmark
    asm("# masking\n");
    double baseline = bench_masking(&value, nvalue, ((1ull << (rand() % 62)) - 1));

    asm("# modulus\n");
    bench_modulus(&value, nvalue, (uint64_t)rand() << 32 | rand(), baseline, false);

    asm("# prime modulus\n");
    for (size_t pp=0; pp < nprimes; pp++) {
        bench_prime(pp, &value, nvalue, baseline);
    }
    printf("\n");

    printf("knuth modulus\n");
    asm("# knuth modulus\n");
    for (size_t pp=0; pp < nprimes; pp++) {
        bench_knuth(pp, &value, nvalue, baseline);
    }

    // to prevent any elision
    FILE *out = fopen("/dev/null", "w");
    fwrite((void*)&value, 1, sizeof(uint64_t), out);
    fclose(out);
}
