//-*-c++-*-
#pragma once

#include <stdint.h>

// murmurhash finalizer, with augmented constants
// http://zimbry.blogspot.com/2011/09/better-bit-mixing-improving-on.html (mix #13)
//
// this function has good avalanche properties and is
// reversible, so it's a bijection between input and output,
// thus will never generate collisions between values.
//
// requires ~5.5 cycles to mix 64-bit number in testing.
static inline uint64_t hashint(uint64_t k) {
    k ^= k >> 30;
    k *= 0xbf58476d1ce4e5b9ull;
    k ^= k >> 27;
    k *= 0x94d049bb133111ebull;
    k ^= k >> 31;
    return k;
}


// Bulk hashing function based off the Fast Positive Hash, but extraneous
// platform dependent code has been removed and everything inlined. Slight
// tweaks to the tail() function to avoid performance penalty trying to do
// unaligned 64-bit load.
//
// Very fast, even for large data (~10GB/s for 256K buffer in tests).
static inline uint64_t hashbuf(const void *data, size_t len) {
    // Copyright (c) 2016-2017 Positive Technologies, https://www.ptsecurity.com,
    // Fast Positive Hash.
    //
    // Portions Copyright (c) 2010-2013 Leonid Yuriev <leo@yuriev.ru>,
    // The 1Hippeus project (t1h).
    //
    // This software is provided 'as-is', without any express or implied
    // warranty. In no event will the authors be held liable for any damages
    // arising from the use of this software.
    //
    // Permission is granted to anyone to use this software for any purpose,
    // including commercial applications, and to alter it and redistribute it
    // freely, subject to the following restrictions:
    //
    // 1. The origin of this software must not be misrepresented; you must not
    //    claim that you wrote the original software. If you use this software
    //    in a product, an acknowledgement in the product documentation would be
    //    appreciated but is not required.
    // 2. Altered source versions must be plainly marked as such, and must not be
    //    misrepresented as being the original software.
    // 3. This notice may not be removed or altered from any source distribution.

    // magic primes
    static const uint64_t p0 = 0xEC99BF0D8372CAABull;
    static const uint64_t p1 = 0x82434FE90EDCEF39ull;
    static const uint64_t p2 = 0xD4F06DB99D67BE4Bull;
    static const uint64_t p3 = 0xBD9CACC22C6E9571ull;
    static const uint64_t p4 = 0x9C06FAF4D023E3ABull;
    static const uint64_t p5 = 0xC060724A8424F345ull;
    static const uint64_t p6 = 0xCB5AF53AE3AAAC31ull;
    static const uint64_t sd = 0X9E3778047C192FE7ull; // largest prime < 2**64/phi

    // rotations
    static const unsigned s0 = 41;
    static const unsigned s1 = 17;
    static const unsigned s2 = 31;

    // bit twiddling helpers
    struct h {
        static uint64_t rot64(uint64_t v, unsigned s) { return (v >> s) | (v << (64 - s));                      }
        static uint64_t mux64(uint64_t v, uint64_t p) { __uint128_t r = (__uint128_t)v*p; return r ^ (r >> 64); }
        static uint64_t mix64(uint64_t v, uint64_t p) { v *= p;  return v ^ rot64(v, s0);                       }

        // load last N <= 8 bytes from v efficiently into uint64_t
        static uint64_t tail(const void *v, size_t tail) {
            uint64_t r = 0;
            const uint8_t *p = (const uint8_t*)v;
            switch (tail & 7) {
                case 0: r |= p[7]; r <<= 8;       // fall through
                case 7: r |= p[6]; r <<= 8;       // fall through
                case 6: r |= p[5]; r <<= 8;       // fall through
                case 5: r |= p[4]; r <<= 32;      // fall through
                case 4: return r + *(uint32_t*)p;
                case 3: r |= p[2]; r <<= 16;      // fall through
                case 2: return r + *(uint16_t*)p;
                case 1: return p[0];
            }
        }
    };

    uint64_t a = sd;
    uint64_t b = len;

    const uint64_t *dptr = (const uint64_t*)data;
    const uint64_t *v;

    if (len > 32) {
        uint64_t c = h::rot64(len, s1) + sd;
        uint64_t d = len ^ h::rot64(sd, s1);

        const void *end = (const char*)dptr + len - 31;
        do {
            v = dptr;
            uint64_t w0 = v[0];  uint64_t w1 = v[1];
            uint64_t w2 = v[2];  uint64_t w3 = v[3];

            uint64_t d02,c13;
            d02 = w0 ^ h::rot64(w2 + d, s1);
            c13 = w1 ^ h::rot64(w3 + c, s1);
            c  +=  a ^ h::rot64(w0, s0);
            d  -=  b ^ h::rot64(w1, s2);
            a  ^= p1 * (d02 + w3);
            b  ^= p0 * (c13 + w2);
            dptr += 4;
        } while (dptr < end);

        a ^= p6 * (h::rot64(c, s1) + d);
        b ^= p5 * (c + h::rot64(d, s1));
        len &= 31;
    }

    v = dptr;
    switch (len) {
        case 32: case 31: case 30: case 29: case 28: case 27: case 26: case 25: b += h::mux64(*v++, p4);            // fall through
        case 24: case 23: case 22: case 21: case 20: case 19: case 18: case 17: a += h::mux64(*v++, p3);            // fall through
        case 16: case 15: case 14: case 13: case 12: case 11: case 10: case  9: b += h::mux64(*v++, p2);            // fall through
        case  8: case  7: case  6: case  5: case  4: case  3: case  2: case  1: a += h::mux64(h::tail(v, len), p1); // fall through
        case  0:
            return h::mux64(h::rot64(a + b, s1), p4) + h::mix64(a ^ b, p0);
    }
}
