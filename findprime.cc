#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <gmp.h>

void ull2mpz(mpz_t z, unsigned long long ull) {
    mpz_import(z, 1, 0, sizeof(ull), 0, 0, &ull);
}

bool isprime(uint64_t nn) {
    mpz_t cc;
    mpz_init(cc);
    ull2mpz(cc, nn);
    return mpz_probab_prime_p(cc, 32); // 1/4^32 < 1e-2015                                                
 }

int main(int argc, const char* argv[]) {
    if (argc < 2) {
        fprintf(stderr, "Usage: %s <number>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    uint64_t num = strtoull(argv[1], NULL, 0);
    printf("num: %zu\n", num);
    
    while (!isprime(num)) {
        num--;
    }

    printf("%llu\n", (long long unsigned)num);    
}
