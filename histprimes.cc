#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <cmath>
#include <algorithm>

#include <bench.h>
#include <primes.h>

int main(int argc, const char* argv[]) {
    srand(time(NULL));
    
    if (argc < 3) {
        fprintf(stderr, "Usage: %s <size> <el/bin>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    size_t size  = atoi(argv[1]);
    size_t elper = atoi(argv[2]);
    
    if (size > 1024*1024) {
        fprintf(stderr, "Can only handle size up to 1M\n");
        exit(EXIT_FAILURE);
    }

    uint64_t *bins = (uint64_t*)malloc(size*sizeof(uint64_t));
    memset(bins, 0, size*sizeof(uint64_t));

    for (size_t ii=0; ii < elper*size; ii++) {
        uint64_t num = (uint64_t)rand() << 32 | rand();
        //uint64_t num=ii;
        bins[knuthmod(num, size)]++;
        
        //Printf: %zd  hash: %zd\n", ii, knuthmod(num, size));
    }

    // compute mean/variance
    size_t n = 0;
    double mean = 0.0;
    double m2   = 0.0;
    size_t max = 0;
    size_t min = size*elper+1;
    for (size_t ii=0; ii < size; ii++) {
        printf("%zd %zd\n", ii, bins[ii]);

        n++;
        double delta = bins[ii] - mean;
        mean += delta/n;
        m2   += delta*(bins[ii]-mean);

        min = std::min(min, bins[ii]);
        max = std::max(max, bins[ii]);
            
    }
    double sigma = sqrt(m2/(n-1));

    printf("# mean: %.3f  sigma: %.6f  min: %zd  max: %zd\n", mean, sigma, min, max);

    size_t noutlier = 0;
    size_t nzero    = 0;
    for (size_t ii=0; ii < size; ii++) {
        if (abs(bins[ii]-mean)/sigma > 3) {
            noutlier++;
        }

        if (bins[ii] == 0) {
            nzero++;
        }
    }

    double percent = (double)noutlier/size*100;
    printf("# outliers: %zd (%.2f%%) [%s] nzero: %zd\n", noutlier, percent, percent < 2.7 ? "good" : "anomaly",  nzero);
    free(bins);
}
