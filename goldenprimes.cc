#include <math.h>
#include <stdint.h>
#include <gmp.h>

#include <vector>

void ull2mpz(mpz_t z, unsigned long long ull) {
    mpz_import(z, 1, 0, sizeof(ull), 0, 0, &ull);
}

bool isprime(uint64_t nn) {
    mpz_t cc;
    mpz_init(cc);
    ull2mpz(cc, nn);
    return mpz_probab_prime_p(cc, 32); // 1/4^32 < 1e-2015                                                
 }

int main(int argc, const char* argv[]) {
    // read in disallowed list
    std::vector<uint64_t> disallowed;
    if (argc > 1) {
        FILE *in = fopen(argv[1], "r");
        if (in == NULL) {
            fprintf(stderr, "Error opening %s, proceeding without disallow list\n", argv[1]);
        } else {
            while (!feof(in)) {
                uint64_t val;
                int ret = fscanf(in, "%llu\n", (unsigned long long*)&val); (void)ret;
                disallowed.push_back(val);
            }
            fclose(in);
        }
    }

    // compute desired targets
    uint64_t targets[106];
    size_t   ntarget = 1;
    targets[0] = 13;
    while (targets[ntarget-1] < 0xFFFFFFFFFFFFFFFF) {
        targets[ntarget] = (uint64_t)(targets[ntarget-1]*1.5);
        if (targets[ntarget] % 2 == 0) {
            targets[ntarget]--;
        }
        ntarget++;
    }


    uint64_t results[106];
    size_t   cnt=0;
    for (size_t kk=0; kk < ntarget; kk++) {
        uint64_t ii = targets[kk];
        while (ii > targets[kk]/2) {  // find next lowest prime from it (will always find one before n/2)
            if (isprime(ii)) {
                fprintf(stderr, "found %zu ", ii);

                bool allowed = true;
                for (size_t nn=0; nn < disallowed.size(); nn++) {
                    if (disallowed[nn] == ii) {
                        allowed = false;
                    }
                }

                if (!allowed) {
                    fprintf(stderr, " [disallowed]\n");
                    ii -= 2;
                    continue;
                } else {
                    fprintf(stderr, " [allowed]\n");
                }

                // give up, we slid back to an old size
                if (kk > 0 && ii == targets[kk-1]) {
                    break;
                }

                // ignore 41
                if (ii == 41) {
                    break;
                }
                
                // save result
                results[cnt] = ii;
                cnt++;
                break;
            }
            ii -= 2; // skip evens
        }
    }

    //const uint32_t alpha32 = 2654435761;              
    //const uint64_t alpha64 = 11400712942368927719ull; // closest prime < 2**64/phi
    
    printf("//-*-c++-*-\n");
    printf("#pragma once\n");
    printf("\n");
    printf("#include <stdint.h>\n");
    printf("#include <unistd.h>\n");
    printf("#include <cassert>\n\n");

    // XXX:
    //printf("__attribute__((noinline))\n");
    printf("static inline uint64_t prime_modulus(size_t pp, uint64_t nn) {\n");
    //printf("    const uint32_t alpha32 = %u;     // closest prime < 2**32/phi\n", alpha32);
    //printf("    const uint64_t alpha64 = %zuull; // closest prime < 2**64/phi\n", alpha64);
    // printf("   static const void* labels[] = {\n");

    // for (size_t ii=0; ii < cnt; ii++) {
    //     printf("       &&label%zd, ", ii);
    //     if (((ii+1) % 12 == 0)) {
    //         printf("\n");
    //     }
    // }
    // printf("\n};\n");
    // printf("\n");
    // printf("    goto *labels[pp];\n");
    // printf("\n");
    
    printf("    switch(pp) {\n");
    for (size_t ii=0; ii < cnt; ii++) {
        printf("        case %3zu: return nn %% %20zuull; break; // %.12f ratio\n", ii, results[ii], ii > 0 ? (double)results[ii]/results[ii-1] : 1.0);

        // if (results[ii] < (1ull << 32)) {
        //     printf("        case %3zu: asm(\"# prime %3zd\\n\"); return (   (uint64_t)%20zuull*(alpha32*nn)) >> 32;  break; // %.12f ratio\n", ii, ii, results[ii], ii > 0 ? (double)results[ii]/results[ii-1] : 1.0);
        // } else {
        //     printf("        case %3zu: asm(\"# prime %3zd\\n\"); return ((__uint128_t)%20zuull*(alpha64*nn)) >> 64; break; // %.12f ratio\n", ii, ii, results[ii], ii > 0 ? (double)results[ii]/results[ii-1] : 1.0);
        //}
    }
    printf("        default: assert(false && \"unknown prime factor\");\n");    
    printf("    }\n");
    printf("}\n");

    printf("\n\n");
    printf("static const size_t   nprimes = %zd;\n", cnt);
    printf("static const uint64_t prime_sizes[nprimes] = {\n");
    for (size_t ii=0; ii < cnt;) {
        printf("    ");
        char buffer[1024];
        for (size_t jj=0; jj < 4 && ii < cnt; jj++, ii++) {
            sprintf(buffer, "%zuull,", results[ii]);
            printf("%-25s ", buffer);
        }
        printf("\n");
    }
    printf("};\n");
}
