CXXOPTS = -I. -Wall -Wextra -O3 -g3

all: primetest verifyprimes testprimes histprimes findprime

test: primetest
	./primetest

asm: primes.h primetest.cc
	$(CXX) $(CXXOPTS) primetest.cc -S -o primetest.s

primetest: primes.h primetest.cc bench.h
	$(CXX) $(CXXOPTS) primetest.cc -o primetest

primes.h: goldenprimes
	./goldenprimes disallowed > primes.h

goldenprimes: goldenprimes.cc disallowed
	$(CXX) $(CXXOPTS) goldenprimes.cc -o goldenprimes -lgmp

verifyprimes: primes.h verifyprimes.cc
	$(CXX) $(CXXOPTS) verifyprimes.cc -o verifyprimes

testprimes: primes.h testprimes.cc
	$(CXX) $(CXXOPTS) testprimes.cc -o testprimes

histprimes: primes.h bench.h histprimes.cc
	$(CXX) $(CXXOPTS) histprimes.cc -o histprimes

findprime: findprime.cc
	$(CXX) $(CXXOPTS) findprime.cc -o findprime -lgmp

clean:
	rm -rf goldenprimes primetest primes.h primetest.s verifyprimes testprimes histprimes findprime benchhash

